Add-PSSnapin Microsoft.Exchange.Management.PowerShell.SnapIn; 
 
$AccessRights = "Reviewer"
$User = "Default"
 
$mailboxes = get-mailbox -Resultsize Unlimited
 
foreach ($mailbox in $mailboxes) {
    $calendarfolders = Get-MailboxFolderStatistics $mailbox.alias | Where-Object{$_.FolderType -eq "Calendar"}
    foreach ( $calendar in $calendarfolders ) {
 
        $identity = $calendar.Identity -Replace ("\\",":\") # Won't work if the calendar is not in the root folder.
 
        Set-MailboxFolderPermission -Identity $identity -User $User -AccessRights $AccessRights
 
    }
}
