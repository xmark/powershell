#requires -version 4
<#
.SYNOPSIS
  This script collects all installed Server Roles and Features
  from all domain-joined Windows Servers.

.DESCRIPTION
  The result is displayed in the console and a report will be
  saved in $HOME\ServerRoles"Date".txt.
  The file will open automatically.

.PARAMETER <Parameter_Name>
  <Brief description of parameter input required. Repeat this attribute if required>

.INPUTS
  <Inputs if any, otherwise state None>

.OUTPUTS
  A text file with the results

.NOTES
  Version:        1.0
  Author:         Patrick Gruenauer | Microsoft MVP PowerShell [2018-2020]
  Web:            https://sid-500.com/2020/04/05/list-all-server-roles-from-all-windows-servers-with-powershell/
  Creation Date:  5 APR 2020
  Purpose/Change: Initial script development

.EXAMPLE
  <Example explanation goes here>
  
  <Example goes here. Repeat this attribute for more than one example>
#>

#---------------------------------------------------------[Script Parameters]------------------------------------------------------

Param (
  #Script parameters go here
)

#---------------------------------------------------------[Initialisations]--------------------------------------------------------

#Set Error Action to Silently Continue
$ErrorActionPreference = 'SilentlyContinue'

#Import Modules & Snap-ins

#----------------------------------------------------------[Declarations]----------------------------------------------------------

#Any Global Declarations go here

#-----------------------------------------------------------[Functions]------------------------------------------------------------



#-----------------------------------------------------------[Execution]------------------------------------------------------------

### Retrieving all Servers by Name
 
$servers = (Get-ADComputer -Properties operatingsystem `
-Filter 'operatingsystem -like "*server*" -and enabled -eq "true"').Name
 
### Collection Point
 
$result = @()
 
### Ping all Member-Server and if ping is successful run
# Get-WindowsFeature
 
foreach ($item in $servers) {
 
	$test = Test-Connection $item -Count 1
	 
	### Providing PowerShell 7 and 5.1 compatibility in terms of return code
	 
	If ($test.Status -eq 'Success' -or $test.StatusCode -eq '0'){
	 
		$roles = Get-WindowsFeature -ComputerName $item |
		Where-Object InstallState -EQ 'Installed'
		 
		### Write into the collection point
		 
		$result += New-Object -TypeName PSObject -Property ([ordered]@{
			'Server' = $item
			'Roles' = $roles.Name -join "`r`n"
			### Escape Character CarriageReturn (`r) + NewLine (`n)
		})
		 
	}
}
 
### Generating Outputs
 
# Collecting Point: Console Output
 
Write-Output $result | Format-Table -Wrap
 
# Text File Output
 
$date = Get-Date -Format MM.dd.yyyy
$result | Format-Table -Wrap |
Out-File "$HOME\ServerRoles$date.txt"
Write-Warning "Output file ServerRoles$date.txt generated in $HOME ... Opening file ..."
 
# Opening File
 
Start-Process $HOME\"ServerRoles$date.txt"