#requires -version 4
<#
.SYNOPSIS
  Disable OneDrive for a specific user

.DESCRIPTION
  Block access for a user on it's own personnal OneDrive Folder 

.PARAMETER <Parameter_Name>
  <Brief description of parameter input required. Repeat this attribute if required>

.INPUTS
  <Inputs if any, otherwise state None>

.OUTPUTS
  <Outputs if any, otherwise state None>

.NOTES
  Version:        1.0
  Author:         kbaseio
  Creation Date:  2022-04-01
  Purpose/Change: Initial script development

.EXAMPLE
  <Example explanation goes here>
  
  <Example goes here. Repeat this attribute for more than one example>
#>

#---------------------------------------------------------[Script Parameters]------------------------------------------------------

Param (
  #Script parameters go here
)

#---------------------------------------------------------[Initialisations]--------------------------------------------------------

#Set Error Action to Silently Continue
$ErrorActionPreference = 'SilentlyContinue'

#Import Modules & Snap-ins
Import-Module Microsoft.Online.SharePoint.Powershell -DisableNameChecking  

#----------------------------------------------------------[Declarations]----------------------------------------------------------

$AdminSiteURL="https://TENANT-admin.sharepoint.com/" 
$OneDriveSiteURL ="https://TENANT-my.sharepoint.com/personal/USER_DOMAIN_TLD" 

#-----------------------------------------------------------[Functions]------------------------------------------------------------



#-----------------------------------------------------------[Execution]------------------------------------------------------------

Connect-SPOService -Url $AdminSiteURL -Credential (Get-Credential) 

Get-SPOSite -Identity $OneDriveSiteURL | Set-SPOSite -LockState NoAccess