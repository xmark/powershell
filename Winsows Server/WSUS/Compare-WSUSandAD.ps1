#requires -version 4
<#
.SYNOPSIS
  Compare WSUS computers against AD list

.DESCRIPTION
   Script compares list of computers registered in WSUS to list from AD. Difference is computers missing from WSUS.

.PARAMETER Log
  Create a LOG file

.PARAMETER WSUSServerHostname
  Specify the name of the WSUS Server. Otherwise the script use a default value. 

.PARAMETER WSUSServerPort
  Specify the port of the WSUS Server. Otherwise the script use a default value. 

.INPUTS
  None

.OUTPUTS
  LOG file if switch parameter is mentioned as parameter

.NOTES
  Version:        1.1
  Author:         kbaseio
  Creation Date:  20220325
  Purpose/Change: Initial script development
  Source : https://gist.github.com/thegooddoctorgonzo/14446d428612eaa38b460bd5b3ff2e0e

.EXAMPLE
  <Example explanation goes here>
  
  <Example goes here. Repeat this attribute for more than one example>
#>

#---------------------------------------------------------[Script Parameters]------------------------------------------------------

Param(

    [Parameter(Mandatory=$false)]
    [Switch]$Log

    [Parameter(Mandatory=$false)]
    [String]$WSUSServerHostname = "localhost"

    [Parameter(Mandatory=$false)]
    [String]$WSUSServerPort = "8530"

    )
#---------------------------------------------------------[Initialisations]--------------------------------------------------------

#Set Error Action to Silently Continue
$ErrorActionPreference = 'SilentlyContinue'

#Import Modules & Snap-ins
Import-Module -Name UpdateServices
Import-Module -Name ActiveDirectory

#----------------------------------------------------------[Declarations]----------------------------------------------------------

$OUList = @(
    "OU=",
    "OU="
)

$WSUSComputers = @()
$ADComputers = @()
$MissingComputers = @()

$LogFileExecutionPath = "C:\Temp\WSUSCompareExecution.log"

#-----------------------------------------------------------[Functions]------------------------------------------------------------



#-----------------------------------------------------------[Execution]------------------------------------------------------------

if($Log) {
    Start-Transcript -Path $LogFileExecutionPath -Append -Force
}

#Get WSUS Server
$WSUSServer = Get-WsusServer $WSUSServerHostname -PortNumber $WSUSServerPort

#Pulling list of WSUS Computers
$WSUSComputers = Get-WsusComputer -UpdateServer $WSUSServer |Select-Object -Property FullDomainName | Sort-Object -Property FullDomainName | foreach {$_.FullDomainName}

#Pulling list of AD computers from each OU                                                                                                                                                                                                                                             
foreach($OU in $OUList){
    $ADComputers += Get-ADComputer -SearchBase  $OU -Filter * | Select-Object -Property DNSHostName | Sort-Object -Property DNSHostName  | foreach {$_.DNSHostName}
}

# Getting all the missing AD Computers from WSUS list
$MissingComputers = $ADComputers | Where-Object {$WSUSComputers -notcontains $_} 

# PRINT OUT MISSING COMPUTER TO END THE SCRIPT FOR NOW
$MissingComputers

if($Log) {
    Stop-Transcript
}