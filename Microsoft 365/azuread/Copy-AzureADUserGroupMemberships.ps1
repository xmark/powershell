#requires -version 4
<#
.SYNOPSIS
  Copy AzureAD Groups from one user to another

.DESCRIPTION
  Get all groups memberships from one user and add another user into each groups
  
.SOURCE 
  https://docs.microsoft.com/en-us/answers/questions/72594/copy-groups-membership-from-a-user-to-a-new-user.html

.PARAMETER SrcUser
  The source user to copy from

.PARAMETER DstUser
  The destination to copy to

.INPUTS
  <Inputs if any, otherwise state None>

.OUTPUTS
  <Outputs if any, otherwise state None>

.NOTES
  Version:        1.0
  Author:         <Name>
  Creation Date:  <Date>
  Purpose/Change: Initial script development

.EXAMPLE
  <Example explanation goes here>
  
  <Example goes here. Repeat this attribute for more than one example>
#>

#---------------------------------------------------------[Script Parameters]------------------------------------------------------

Param (
    [string]$SrcUser, 
    [string]$DstUser
)

#---------------------------------------------------------[Initialisations]--------------------------------------------------------

#Set Error Action to Silently Continue
$ErrorActionPreference = 'SilentlyContinue'

#Import Modules & Snap-ins

#----------------------------------------------------------[Declarations]----------------------------------------------------------

#Any Global Declarations go here

#-----------------------------------------------------------[Functions]------------------------------------------------------------



#-----------------------------------------------------------[Execution]------------------------------------------------------------

$Groups = Get-AzureADUserMembership -ObjectId {$SrcUser}

foreach($Group in $Groups){ 
	Add-AzureADGroupMember -ObjectId $Group.ObjectId -RefObjectId {$DstUser} 
}